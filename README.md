# Un chaton à la fac


## Présentation

L'idée ici est de surfer sur la vague https://degooglisons-internet.org/ et de mettre en place un http://chatons.org/ dans une université.

Un CHATON est un ensemble de services cloud hébergés avec des critères éthiques, dont le fait de ne pas exploiter les données de ses utilisateurs.

Les universités sont le point d'entrée de nombreux·se·s jeunes dans le monde du numérique ainsi que le lieu de travail de nombreux·se·s personnels qui font usage des services numériques de leur organisation.
Il serait pertinent que de telles organisation mettent à disposition des services de cloud protégeant les données de leurs étudiants et personnels.

## Brainhurricane

Les premières discussions sont sur le pad https://annuel.framapad.org/p/chaton-upsud

après une première évocation sur Framacolibris https://framacolibri.org/t/framacolibris-permettre-aux-universites-de-devenir-fournisseurs-de-services-chatons/1692/

## Feuille de route

(en cours de mise en place)

- premier jet d'installations et mise à disposition pour attirer l'intérêt d'utilisateurs
- mise en place d'une page d'entrée
- diffusion et mise en place de CHATONS dans d'autres universités ?

## Comment contribuer ?

Ce projet est ouvert à la contribution sous formes d'idées, de souhaits, de retours d'utilisateurs, de remontée de bugs, d'encouragements, de pull requests avec le code que vous avez développé avec virtuosité.

On compte sur vous pour être sympa et on fera de notre mieux pour intégrer tout ce qui peut raisonnablement être intégré.