# Un CHATON à la fac ?

Un ensemble d'éléments sont à mettre en place :

- [ ] référencer la charte https://chatons.org/fr/charte-et-manifeste
- [ ] référencer le manifeste https://chatons.org/fr/charte-et-manifeste
- [ ] afficher "Les services proposés sont hébergés sur un serveur installé en <région> et propriété de <propriétaire>."
- [ ] documenter et mettre en ligne un "carnet d'install" des services proposés
- [ ] mettre en place un backup
- [ ] publier des statistiques d'usage anonymisées
- [ ] publier un journal des incidents (ex: status.<adresse>.org)
- [ ] afficher une description de l'offre de service
- [ ] afficher les tarifs de l'offre
- [ ] afficher les CGU
- [ ] clause « Données personnelles et respect de la vie privée dans les CGU
- [ ] rendre publics comptes et rapports d'activité relatifs au service CHATONS
- [ ] afficher la politique de sécurité et de sauvegarde
- [ ] publier la liste des paquets installés
- [ ] chiffrer : 1.https 2. end-to-end
- [ ] mettre en place un canary

Dans le cadre de la mise en place sur les serveurs d'une université, le critère suivant doit mener à une décision :

- [ ] ne pas mettre de restriction _a priori_ à l'accès aux services (ne pas mettre de restriction sur le profil des utilisateurs)

Quelle organisation adopter ?

- option1 : mise en place d'un CHATONS complet au sein d'une université
-option2 : 

    + mise à disposition de certains services (pads, talk) en mode CHATONS
    + mise en place de services hébergés par l'université à destination des étudiants et personnels (non CHATONS)
    
L'option 2 a pour particularité d'inciter les services informatiques des universités à reprendre la main sur l'hébergement des services cloud, sans pour autant relever du CHATONS,

en mettant à disposition au sein d'un CHATONS un certain nombre de services pouvant être employés par tous les profils d'utilisateur·rice·s.